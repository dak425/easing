package ffmpeg

import (
	"fmt"
	"math"
)

type EasingStrFunc func(string) string

func FuncFromStr(name string) (EasingStrFunc, error) {
	switch name {
	case "linear":
		return LinearStr, nil
	case "easeInSine":
		return EaseInSineStr, nil
	case "easeOutSine":
		return EaseOutSineStr, nil
	case "easeInOutSine":
		return EaseInOutSineStr, nil
	case "easeInQuad":
		return EaseInQuadStr, nil
	case "easeOutQuad":
		return EaseOutQuadStr, nil
	case "easeInOutQuad":
		return EaseInOutQuadStr, nil
	case "easeInCubic":
		return EaseInCubicStr, nil
	case "easeInQuart":
		return EaseInQuartStr, nil
	case "easeOutQuart":
		return EaseOutQuartStr, nil
	case "easeInOutQuart":
		return EaseInOutQuartStr, nil
	case "easeInQuint":
		return EaseInQuintStr, nil
	case "easeOutQuint":
		return EaseOutQuintStr, nil
	case "easeInOutQuint":
		return EaseInOutQuintStr, nil
	case "easeInExpo":
		return EaseInExpoStr, nil
	case "easeOutExpo":
		return EaseOutExpoStr, nil
	case "easeInOutExpo":
		return EaseInOutExpoStr, nil
	case "easeInCirc":
		return EaseInCircStr, nil
	case "easeOutCirc":
		return EaseOutCircStr, nil
	case "easeInOutCirc":
		return EaseInOutCircStr, nil
	case "easeInBack":
		return EaseInBackStr, nil
	case "easeOutBack":
		return EaseOutBackStr, nil
	case "easeInOutBack":
		return EaseInOutBackStr, nil
	case "easeInElastic":
		return EaseInElasticStr, nil
	case "easeOutElastic":
		return EaseOutElasticStr, nil
	case "easeInOutElastic":
		return EaseInOutElasticStr, nil
	case "easeInBounce":
		return EaseInBounceStr, nil
	case "easeOutBounce":
		return EaseOutBounceStr, nil
	case "easeInOutBounce":
		return EaseInOutBounceStr, nil
	default:
		return nil, fmt.Errorf("invalid easing func name given: '%s'", name)
	}
}

func LinearStr(time string) string {
	return time
}

func EaseInSineStr(time string) string {
	return fmt.Sprintf("1-cos(((%s)*PI)/2)", time)
}

func EaseOutSineStr(time string) string {
	return fmt.Sprintf("sin(((%s)*PI)/2)", time)
}

func EaseInOutSineStr(time string) string {
	return fmt.Sprintf("-(cos(PI*(%s))-1)/2", time)
}

func EaseInQuadStr(time string) string {
	return fmt.Sprintf("pow((%s)\\,2)", time)
}

func EaseOutQuadStr(time string) string {
	return fmt.Sprintf("1-(1-(%s))*(1-(%s))", time, time)
}

func EaseInOutQuadStr(time string) string {
	return fmt.Sprintf(
		"if(lt((%s)\\,0.5)\\,2*pow((%s)\\,2)\\,1-pow(-2*(%s)+2\\,2)/2)",
		time,
		time,
		time,
	)
}

func EaseInCubicStr(time string) string {
	return fmt.Sprintf("pow((%s)\\,3)", time)
}

func EaseOutCubicStr(time string) string {
	return fmt.Sprintf("1-pow(1-(%s)\\,3)", time)
}

func EaseInOutCubicStr(time string) string {
	return fmt.Sprintf(
		"if(lt((%s)\\,0.5)\\,4*pow((%s)\\,3)\\,1-pow(-2*(%s)+2\\,3)/2)",
		time,
		time,
		time,
	)
}

func EaseInQuartStr(time string) string {
	return fmt.Sprintf("pow((%s)\\,4)", time)
}

func EaseOutQuartStr(time string) string {
	return fmt.Sprintf("1-pow(1-(%s)\\,4)", time)
}

func EaseInOutQuartStr(time string) string {
	return fmt.Sprintf(
		"if(lt((%s)\\,0.5)\\,8*pow((%s)\\,4)\\,1-pow(-2*(%s)+2\\,4)/2)",
		time,
		time,
		time,
	)
}

func EaseInQuintStr(time string) string {
	return fmt.Sprintf("pow((%s)\\,5)", time)
}

func EaseOutQuintStr(time string) string {
	return fmt.Sprintf("1-pow(1-(%s)\\,5)", time)
}

func EaseInOutQuintStr(time string) string {
	return fmt.Sprintf(
		"if(lt((%s)\\,0.5)\\,16*pow((%s)\\,5)\\,1-pow(-2*(%s)+2\\,5)/2)",
		time,
		time,
		time,
	)
}

func EaseInExpoStr(time string) string {
	return fmt.Sprintf(
		"if(eq((%s)\\,0)\\,0\\,pow(2\\,10*(%s)-10))",
		time,
		time,
	)
}

func EaseOutExpoStr(time string) string {
	return fmt.Sprintf(
		"if(eq((%s)\\,1)\\,1\\,1-pow(2\\,-10*(%s)))",
		time,
		time,
	)
}

func EaseInOutExpoStr(time string) string {
	firstExpr := fmt.Sprintf("pow(2\\,20*(%s)-10)/2", time)
	secondExpr := fmt.Sprintf("(2-pow(2\\,-20*(%s)+10))/2", time)
	return fmt.Sprintf(
		"if(eq((%s)\\,0)\\,0\\,(if(eq((%s)\\,1)\\,1\\,if(lt((%s)\\,0.5)\\,pow(2\\,20*(%s)-10)/2)\\,(2-pow(2\\,-20*(%s)+10))/2)))",
		time,
		time,
		time,
		firstExpr,
		secondExpr,
	)
}

func EaseInCircStr(time string) string {
	return fmt.Sprintf("1-sqrt(1-pow((%s)\\,2))", time)
}

func EaseOutCircStr(time string) string {
	return fmt.Sprintf("sqrt(1-pow((%s)-1\\,2))", time)
}

func EaseInOutCircStr(time string) string {
	return fmt.Sprintf(
		"if(lt((%s)\\,0.5)\\,(1-sqrt(1-pow(2*(%s)\\,2)))/2\\,(sqrt(1-pow(-2*(%s)+2\\,2))+1)/2)",
		time,
		time,
		time,
	)
}

func EaseInBackStr(time string) string {
	c1 := 1.70158
	c3 := c1 + 1
	return fmt.Sprintf("%f*pow((%s)\\,3)-%f*pow((%s)\\,2)", c1, time, c3, time)
}

func EaseOutBackStr(time string) string {
	c1 := 1.70158
	c3 := c1 + 1
	return fmt.Sprintf("1+%f*pow((%s)-1\\,3)+%f*pow((%s)-1\\,2)", c3, time, c1, time)
}

func EaseInOutBackStr(time string) string {
	c1 := 1.70158
	c2 := c1 * 1.525
	return fmt.Sprintf(
		"if(lt((%s)\\,0.5)\\,(pow(2*(%s)\\,2)*((%f+1)*2*(%s)-%f))/2\\,(pow(2*(%s)-2\\,2)*((%f+1)*((%s)*2-2)+%f)+2)/2)",
		time,
		time,
		c2,
		time,
		c2,
		time,
		c2,
		time,
		c2,
	)
}

func EaseInElasticStr(time string) string {
	c4 := (2 * math.Pi) / 3
	return fmt.Sprintf(
		"if(eq((%s)\\,0)\\,0\\,if(eq((%s)\\,1)\\,1\\,-pow(2\\,10*(%s)-10)*sin((%s)*10-10.75)*%f))",
		time,
		time,
		time,
		time,
		c4,
	)
}

func EaseOutElasticStr(time string) string {
	c4 := (2 * math.Pi) / 3
	return fmt.Sprintf(
		"if(eq((%s)\\,0)\\,0\\,if(eq((%s)\\,1)\\,1\\,pow(2\\,-10*(%s))*sin(((%s)*10-0.75)*%f)+1))",
		time,
		time,
		time,
		time,
		c4,
	)
}

func EaseInOutElasticStr(time string) string {
	c5 := (2 * math.Pi) / 4.5
	ltExpr := fmt.Sprintf("-(pow(2\\,20*(%s)-10)*sin((20*(%s)-11.125)*%f))/2", time, time, c5)
	gtExpr := fmt.Sprintf("(pow(2\\,-20*(%s)+10)*sin((20*(%s)-11.125)*%f))/2+1", time, time, c5)
	return fmt.Sprintf(
		"if(eq((%s)\\,0)\\,0\\,if(eq((%s)\\,1)\\,1\\,if(lt((%s)\\,0.5)\\,%s\\,%s)))",
		time,
		time,
		time,
		ltExpr,
		gtExpr,
	)
}

func EaseInBounceStr(time string) string {
	x := fmt.Sprintf("1-(%s)", time)
	return fmt.Sprintf("1-(%s)", EaseOutBounceStr(x))
}

func EaseOutBounceStr(time string) string {
	n1 := 7.5625
	d1 := 2.75
	firstExpr := fmt.Sprintf("%f*pow((%s)\\,2)", n1, time)
	secondTime := fmt.Sprintf("((%s)-1.5)", time)
	secondExpr := fmt.Sprintf("%f*(%s/%f)*(%s)+0.75", n1, secondTime, d1, secondTime)
	thirdTime := fmt.Sprintf("((%s)-2.25)", time)
	thirdExpr := fmt.Sprintf("%f*(%s/%f)*(%s)+0.9375", n1, thirdTime, d1, thirdTime)
	fourthTime := fmt.Sprintf("((%s)-2.65)", time)
	fourthExpr := fmt.Sprintf("%f*(%s/%f)*(%s)+0.984375", n1, fourthTime, d1, fourthTime)
	return fmt.Sprintf(
		"if(lt((%s)\\, 1/%f)\\,%s\\,if(lt((%s)\\,2/%f)\\,%s\\,if(lt((%s)\\,2.5/%f)\\,%s\\,%s)))",
		time,
		d1,
		firstExpr,
		time,
		d1,
		secondExpr,
		time,
		d1,
		thirdExpr,
		fourthExpr,
	)
}

func EaseInOutBounceStr(time string) string {
	x1 := fmt.Sprintf("1-2*(%s)", time)
	x2 := fmt.Sprintf("2*(%s)-1", time)
	gtExpr := fmt.Sprintf("(1-(%s))/2", EaseOutBounceStr(x1))
	ltExpr := fmt.Sprintf("(1+(%s))/2", EaseOutBounceStr(x2))
	return fmt.Sprintf(
		"if(lt((%s)\\,0.5)\\,%s\\,%s",
		time,
		ltExpr,
		gtExpr,
	)
}
